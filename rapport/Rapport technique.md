# Rapport sur le Socle Technique de l'Application de Gestion d'Agence de Voyage

## Introduction

Le présent rapport expose le socle technique utilisé pour le développement de l'application de gestion d'agence de voyage,
dans le cadre du projet de Licence 3 en Informatique. L'application est développée en utilisant le framework Spring Boot
et une base de données PostgreSQL.

## Spring Boot

![Spring Boot](https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Spring_Framework_Logo_2018.svg/1200px-Spring_Framework_Logo_2018.svg.png "Spring Boot")

**Spring Boot** a été choisi comme framework principal pour le développement de l'application en raison de ses nombreux avantages. Il simplifie le processus de développement en offrant une configuration par défaut tout en permettant une personnalisation facile. De plus, Spring Boot favorise une approche conventionnelle plutôt que configuration, accélérant ainsi le cycle de développement.

## PostgreSQL

<div align="center">
  <img src="https://cdn.iconscout.com/icon/free/png-256/free-postgresql-11-1175122.png" />
</div>

### PostgreSQL comme Système de Gestion de Base de Données (SGBD)

La sélection de PostgreSQL comme stratégie repose sur des considérations telles que la performance, les fonctionnalités, la licence et le soutien de la communauté. L'intégration de PostgreSQL avec Spring Boot fournit une base de données robuste et performante, parfaitement adaptée aux exigences complexes de l'application de gestion d'agence de voyage.

### Intégration de PostgreSQL avec Spring Boot

La configuration de la source de données **PostgreSQL** dans le fichier **application.yml** illustre la simplicité de l'intégration de **PostgreSQL** avec **Spring Boot**. Cette simplicité facilite le processus de développement et garantit une compatibilité transparente entre l'application Java et la base de données **PostgreSQL**.

```yaml
spring:
  datasource:
    driver-class-name: org.postgresql.Driver
    url: jdbc:postgresql://localhost:5432/test
    username: postgres
    password: admin
```
* **driver-class-name:** C'est la classe du pilote JDBC que Spring utilisera pour se connecter à la base de données. Dans ce cas, il s'agit de la classe de pilote PostgreSQL.
* **url:** C'est l'URL de la base de données à laquelle l'application se connectera. Dans cet exemple, l'URL JDBC pour PostgreSQL est utilisée, avec le nom d'hôte _**localhost**_ et le port _**5432**_. **_test_** est le nom de la base de données.
* **username:** C'est le nom d'utilisateur que l'application utilisera pour se connecter à la base de données. Dans ce cas, c'est **_postgres_**.
* **password:** C'est le mot de passe associé au nom d'utilisateur pour l'authentification à la base de données. Dans cet exemple, le mot de passe est **_admin_**.

## Maven

![Maven]( https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Apache_Maven_logo.svg/1024px-Apache_Maven_logo.svg.png "Maven")

### Gestionnaire de Dépendances

**Maven** simplifie la gestion des dépendances en téléchargeant automatiquement les bibliothèques nécessaires à partir du référentiel central de Maven. Ceci est spécifié dans la section **dependencies** du fichier **pom.xml**. L'utilisation de Maven permet une gestion efficace des versions des bibliothèques, simplifiant ainsi le processus de développement.

Le fichier **pom.xml** (Project Object Model) joue un rôle essentiel dans la gestion des dépendances, la configuration du projet, et la construction de l'application. Voici quelques points clés de ce fichier:

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <scope>runtime</scope>
    </dependency>
</dependencies>
```

* **Dépendance Spring Boot Data JPA:** Cette dépendance inclut les bibliothèques nécessaires pour utiliser JPA avec Spring Boot. Elle configure automatiquement Hibernate comme fournisseur JPA par défaut.

* **Dépendance PostgreSQL:** Cette dépendance permet l'utilisation de PostgreSQL en tant que base de données avec Spring Boot.


