# Diagramme de class et Modèle relationnel de données

## Diagramme de class

![Diagramme de class](https://gitlab.com/remilayanis/gestion-agence-de-voyage/-/raw/develope/rapport/diagramme_de_class.png?ref_type=heads "Diagramme de class")

## Modèle relationnel de données

Le modèle relationnel proposé comprend les tables **client**, **destination**, **employer** et **voyage** formant une base de données pour la gestion de voyages.
Les clés primaires et étrangères sont utilisées pour établir des liens entre ces tables, assurant une organisation structurée des informations relatives aux clients, destinations, employés et voyages.
Ce modèle offre une fondation solide pour le stockage cohérent et la récupération efficace des données liées au domaine du voyage.

* **client**( **id**, adresse, email, nom, numero_telephone, prenom )

* **destination**( **id**, capacite, date_debut, date_fin, description, nom, pays, places_disponibles, prix_par_personne, region, type_voyage )

* **employer**( **id**, etat, password, role, username )

* **voyage**( **id**, date_depart, date_retour, nombre_personnes, totalapayer, **#client_id**, **#destination_id** )

<div style="page-break-after: always;"></div>

# Modèle de Base de Données pour une Agence de Voyages

## Création de la base de données 

Ces scripts SQL définissent la structure de plusieurs tables liées entre elles pour notre application de gestion de voyages.
Voici une description de chaque table :

### Table "client" :
* Cette table stocke des informations sur les clients de l'agence de voyages.
* Champs :
    * ***id :*** Identifiant unique du client (clé primaire).
    * ***nom :*** Nom du client.
    * ***prenom :*** Prénom du client.
    * ***adresse :*** Adresse du client.
    * ***numero_telephone :*** Numéro de téléphone du client.
    * ***email :*** Adresse e-mail du client.

```sql
CREATE TABLE client (
    id serial PRIMARY KEY,
    nom varchar(255) NOT NULL,
    prenom varchar(255) NOT NULL,
    adresse varchar(255) NOT NULL,
    numero_telephone varchar(255) NOT NULL,
    email varchar(255) NOT NULL
);
```

### Table "employer" :
* Cette table stocke les informations sur les employés de l'agence de voyages.
* Champs :
    * ***id :*** Identifiant unique de l'employé (clé primaire).
    * ***username :*** Nom d'utilisateur de l'employé.
    * ***password :*** Mot de passe de l'employé.
    * ***role :*** Rôle de l'employé.
    * ***etat :*** État de l'employé.

```sql
CREATE TABLE employer (
    id serial PRIMARY KEY,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    role varchar(255) NOT NULL,
    etat varchar(255) NOT NULL
);
```

<div style="page-break-after: always;"></div>

### Table "destination" :
* Cette table contient des détails sur les destinations proposées pour les voyages.
* Champs :
    * ***id :*** Identifiant unique de la destination (clé primaire).
    * ***nom :*** Nom de la destination.
    * ***description :*** Description de la destination.
    * ***date_debut :*** Date de début du voyage.
    * ***date_fin :*** Date de fin du voyage.
    * ***region :*** Région de la destination.
    * ***type_voyage :*** Type de voyage.
    * ***pays :*** Pays de la destination.
    * ***capacite :*** Capacité d'accueil de la destination.
    * ***places_disponibles :*** Nombre de places disponibles.
    * ***prix_par_personne :*** Prix par personne pour le voyage.

```sql
CREATE TABLE destination (
    id serial PRIMARY KEY,
    nom varchar(255) NOT NULL,
    description varchar(255) NOT NULL,
    date_debut date NOT NULL,
    date_fin date NOT NULL,
    region varchar(255) NOT NULL,
    type_voyage varchar(255) NOT NULL,
    pays varchar(255) NOT NULL,
    capacite int4 NOT NULL,
    places_disponibles int4 NOT NULL,
    prix_par_personne float8 NOT NULL
);
```

### Table "destination_images" :
* Cette table associe des images aux destinations.
* Champs :
    * ***destination_id :*** Identifiant de la destination auquel l'image est associée (clé étrangère faisant référence à la table "destination").
    * ***image :*** Chemin de l'image associée.

```sql
CREATE TABLE destination_images (
    destination_id int8 NOT NULL,
    image varchar(255) NOT NULL,
    CONSTRAINT fk_destination FOREIGN KEY (destination_id) REFERENCES destination(id)
);
```

<div style="page-break-after: always;"></div>

### Table "voyage" :
* Cette table enregistre les détails des voyages réservés par les clients.
* Champs :
    * ***id :*** Identifiant unique du voyage (clé primaire).
    * ***date_depart :*** Date de départ du voyage.
    * ***date_retour :*** Date de retour du voyage.
    * ***nombre_personnes :*** Nombre de personnes participant au voyage.
    * ***totalapayer :*** Montant total à payer pour le voyage.
    * ***client_id :*** Identifiant du client associé au voyage (clé étrangère faisant référence à la table "client").
    * ***destination_id :*** Identifiant de la destination associée au voyage (clé étrangère faisant référence à la table "destination").

```sql
CREATE TABLE voyage (
    id serial PRIMARY KEY,
    date_depart date NOT NULL,
    date_retour date NOT NULL,
    nombre_personnes int4 NOT NULL,
    totalapayer float8 NOT NULL,
    client_id int8 NULL,
    destination_id int8 NULL,
    CONSTRAINT fk_client FOREIGN KEY (client_id) REFERENCES client(id),
    CONSTRAINT fk_destination FOREIGN KEY (destination_id) REFERENCES destination(id)
);
```
 
Ces tables offrent une base solide pour la gestion des clients, des employés, des destinations et des voyages au sein de l'agence de voyages. Les clés étrangères établissent des liens entre les différentes entités, permettant une organisation structurée des données.

## Ajouter un Voyage

La requête SQL suivante permet d'ajouter un nouveau voyage à la base de données de l'agence de voyages, en spécifiant les informations essentielles telles que la date de départ, la date de retour, le nombre de personnes, le montant total à payer, l'ID du client et l'ID de la destination.

```sql
    INSERT INTO voyage (date_depart, date_retour, nombre_personnes, totalapayer, client_id, destination_id) 
    VALUES ('?', '?', ?, ?, ?, ?);
```

<div style="page-break-after: always;"></div>

## Rechercher une destination par Critères

La requête SQL suivante permet de rechercher les informations d'une destination dans la base de données de l'agence de voyages en se basant sur des critères précis tels que le nom, le pays, la région et le type de voyage.

```sql
    SELECT *
    FROM destination
    WHERE nom = '?' AND pays = '?' AND region = '?' AND type_voyage = '?';
```

## Rechercher les voyages d'un client

La requête SQL suivante permet de récupérer tous les voyages d'un client en se basant sur l'identifiant unique (ID) du client.

```sql
    SELECT *
    FROM voyage
    WHERE client_id = ?;
```

## Mise à Jour des Informations d'un Employé

La requête SQL ci-dessous permet de mettre à jour les informations d'un employé dans la base de données de l'agence de voyages en se basant sur l'identifiant unique (ID) de l'employé.

```sql
    UPDATE employer
    SET etat = '?', password = '?', role = '?', username = '?'
    WHERE id = ?;
```

## Suppression de Client par ID

La requête SQL ci-dessous permet de supprimer de manière précise un client par son identifiant.

```sql
    DELETE FROM client
    WHERE id = ?;
```