package com.example.gestionagencevoyage.repository;

import com.example.gestionagencevoyage.entity.EmployerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployerRepository extends JpaRepository<EmployerEntity, Long> {
    EmployerEntity findByUsernameAndPassword(String username, String password);
}
