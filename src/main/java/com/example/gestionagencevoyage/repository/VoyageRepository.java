package com.example.gestionagencevoyage.repository;

import com.example.gestionagencevoyage.entity.VoyageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoyageRepository extends JpaRepository<VoyageEntity, Long> {
}