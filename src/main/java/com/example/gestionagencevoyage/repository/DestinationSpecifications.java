package com.example.gestionagencevoyage.repository;

import com.example.gestionagencevoyage.entity.DestinationEntity;
import com.example.gestionagencevoyage.entity.PaysEnum;
import com.example.gestionagencevoyage.entity.RegionEnum;
import com.example.gestionagencevoyage.entity.TypeVoyageEnum;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;

public class DestinationSpecifications {

    public static Specification<DestinationEntity> findByCriteria(
            PaysEnum pays,
            RegionEnum region,
            TypeVoyageEnum typeVoyage,
            LocalDate dateDebut,
            LocalDate dateFin,
            int placesDisponibles,
            double prixParPersonne) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.and(
                pays != null ? criteriaBuilder.equal(root.get("pays"), pays) : criteriaBuilder.conjunction(),
                region != null ? criteriaBuilder.equal(root.get("region"), region) : criteriaBuilder.conjunction(),
                typeVoyage != null ? criteriaBuilder.equal(root.get("typeVoyage"), typeVoyage) : criteriaBuilder.conjunction(),
                dateDebut != null ? criteriaBuilder.lessThanOrEqualTo(root.get("dateDebut"), dateDebut) : criteriaBuilder.conjunction(),
                dateFin != null ? criteriaBuilder.greaterThanOrEqualTo(root.get("dateFin"), dateFin) : criteriaBuilder.conjunction(),
                placesDisponibles != 0 ? criteriaBuilder.greaterThanOrEqualTo(root.get("placesDisponibles"), placesDisponibles) : criteriaBuilder.conjunction(),
                prixParPersonne != 0 ? criteriaBuilder.lessThanOrEqualTo(root.get("prixParPersonne"), prixParPersonne) : criteriaBuilder.conjunction()
        );
    }
}