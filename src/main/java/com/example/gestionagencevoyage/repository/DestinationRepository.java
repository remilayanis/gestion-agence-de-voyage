package com.example.gestionagencevoyage.repository;

import com.example.gestionagencevoyage.entity.DestinationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface DestinationRepository extends JpaRepository<DestinationEntity, Long>, JpaSpecificationExecutor<DestinationEntity> {
}
