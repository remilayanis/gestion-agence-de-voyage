package com.example.gestionagencevoyage.entity;

import java.util.ArrayList;
import java.util.List;

public enum PaysEnum {
    // Europe
    FRANCE("France", RegionEnum.EUROPE),
    ALLEMAGNE("Allemagne", RegionEnum.EUROPE),
    ESPAGNE("Espagne", RegionEnum.EUROPE),
    ITALIE("Italie", RegionEnum.EUROPE),
    GRECE("Grèce", RegionEnum.EUROPE),

    // Amérique du Nord
    ETATS_UNIS("États-Unis", RegionEnum.AMERIQUE_DU_NORD),
    CANADA("Canada", RegionEnum.AMERIQUE_DU_NORD),
    MEXIQUE("Mexique", RegionEnum.AMERIQUE_DU_NORD),
    COSTA_RICA("Costa Rica", RegionEnum.AMERIQUE_DU_NORD),
    JAMAIQUE("Jamaïque", RegionEnum.AMERIQUE_DU_NORD),

    // Amérique du Sud
    BRESIL("Brésil", RegionEnum.AMERIQUE_DU_SUD),
    ARGENTINE("Argentine", RegionEnum.AMERIQUE_DU_SUD),
    PEROU("Pérou", RegionEnum.AMERIQUE_DU_SUD),
    CHILI("Chili", RegionEnum.AMERIQUE_DU_SUD),
    COLOMBIE("Colombie", RegionEnum.AMERIQUE_DU_SUD),

    // Asie
    CHINE("Chine", RegionEnum.ASIE),
    JAPON("Japon", RegionEnum.ASIE),
    INDE("Inde", RegionEnum.ASIE),
    THAILANDE("Thaïlande", RegionEnum.ASIE),
    COREE_DU_SUD("Corée du Sud", RegionEnum.ASIE),

    // Afrique
    ALGERIE("Algérie", RegionEnum.AFRIQUE),
    KENYA("Kenya", RegionEnum.AFRIQUE),
    AFRIQUE_DU_SUD("Afrique du Sud", RegionEnum.AFRIQUE),
    NIGERIA("Nigeria", RegionEnum.AFRIQUE),
    EGYPTE("Égypte", RegionEnum.AFRIQUE),

    // Océanie
    AUSTRALIE("Australie", RegionEnum.OCEANIE),
    NOUVELLE_ZELANDE("Nouvelle-Zélande", RegionEnum.OCEANIE),
    FIJI("Fidji", RegionEnum.OCEANIE),
    PAPUA_NOUVELLE_GUINEE("Papouasie-Nouvelle-Guinée", RegionEnum.OCEANIE),
    VANUATU("Vanuatu", RegionEnum.OCEANIE),

    // Moyen-Orient
    ARABIE_SAOUDITE("Arabie Saoudite", RegionEnum.MOYEN_ORIENT),
    EMIRATS_ARABES_UNIS("Émirats arabes unis", RegionEnum.MOYEN_ORIENT),
    TURQUIE("Turquie", RegionEnum.MOYEN_ORIENT),
    IRAN("Iran", RegionEnum.MOYEN_ORIENT),

    // Caraïbes
    JAMAIQUE_CARAIBES("Jamaïque", RegionEnum.CARAIBES),
    BAHAMAS("Bahamas", RegionEnum.CARAIBES),
    REPUBLIQUE_DOMINICAINE("République dominicaine", RegionEnum.CARAIBES),
    CUBA("Cuba", RegionEnum.CARAIBES),
    TRINITE_ET_TOBAGO("Trinité-et-Tobago", RegionEnum.CARAIBES),

    // Amérique centrale
    COSTA_RICA_CENTRAL("Costa Rica", RegionEnum.AMERIQUE_CENTRALE),
    NICARAGUA("Nicaragua", RegionEnum.AMERIQUE_CENTRALE),
    GUATEMALA("Guatemala", RegionEnum.AMERIQUE_CENTRALE),
    BELIZE("Belize", RegionEnum.AMERIQUE_CENTRALE),
    HONDURAS("Honduras", RegionEnum.AMERIQUE_CENTRALE),

    // Antarctique
    ANTARCTIQUE("Antarctique", RegionEnum.ANTARCTIQUE);

    private final String nom;
    private final RegionEnum region;

    PaysEnum(String nom, RegionEnum region) {
        this.nom = nom;
        this.region = region;
    }

    public String getNom() {
        return nom;
    }

    public RegionEnum getRegion() {
        return region;
    }

    public static List<PaysEnum> getPaysByRegion(RegionEnum region) {
        List<PaysEnum> paysList = new ArrayList<>();
        for (PaysEnum pays : values()) {
            if (pays.getRegion() == region) {
                paysList.add(pays);
            }
        }
        return paysList;
    }
}
