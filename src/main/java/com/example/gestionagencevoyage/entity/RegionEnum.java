package com.example.gestionagencevoyage.entity;

public enum RegionEnum {
    EUROPE("Europe"),
    AMERIQUE_DU_NORD("Amérique du Nord"),
    AMERIQUE_DU_SUD("Amérique du Sud"),
    ASIE("Asie"),
    AFRIQUE("Afrique"),
    OCEANIE("Océanie"),
    MOYEN_ORIENT("Moyen-Orient"),
    CARAIBES("Caraïbes"),
    AMERIQUE_CENTRALE("Amérique centrale"),
    ANTARCTIQUE("Antarctique");

    private final String displayName;

    RegionEnum(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}