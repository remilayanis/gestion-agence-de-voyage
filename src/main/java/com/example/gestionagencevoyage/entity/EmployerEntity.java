package com.example.gestionagencevoyage.entity;

import javax.persistence.*;

@Entity
@Table(name = "employer")
public class EmployerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private RoleEnum role;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EtatCompteEnum etat;

    public EmployerEntity() {
    }

    public EmployerEntity(String username, String password, RoleEnum role, EtatCompteEnum etat) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.etat = etat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public EtatCompteEnum getEtat() {
        return etat;
    }

    public void setEtat(EtatCompteEnum etat) {
        this.etat = etat;
    }

    @Override
    public String toString() {
        return "EmployerEntity{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", etat=" + etat +
                '}';
    }
}
