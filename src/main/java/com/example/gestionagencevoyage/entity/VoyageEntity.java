package com.example.gestionagencevoyage.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "voyage")
public class VoyageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "client_id", foreignKey = @ForeignKey(name = "fk_client"))
    private ClientEntity client;

    @ManyToOne
    @JoinColumn(name = "destination_id", foreignKey = @ForeignKey(name = "fk_destination"))
    private DestinationEntity destination;

    @Column(nullable = false)
    private LocalDate dateDepart;
    @Column(nullable = false)
    private LocalDate dateRetour;
    @Column(nullable = false)
    private int nombrePersonnes;
    @Column(nullable = false)
    private double totalAPayer;

    public VoyageEntity() {
    }

    public VoyageEntity(Long id, ClientEntity client, DestinationEntity destination, LocalDate dateDepart, LocalDate dateRetour, int nombrePersonnes, double totalAPayer) {
        this.id = id;
        this.client = client;
        this.destination = destination;
        this.dateDepart = dateDepart;
        this.dateRetour = dateRetour;
        this.nombrePersonnes = nombrePersonnes;
        this.totalAPayer = totalAPayer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientEntity getClient() {
        return client;
    }

    public void setClient(ClientEntity client) {
        this.client = client;
    }

    public DestinationEntity getDestination() {
        return destination;
    }

    public void setDestination(DestinationEntity destination) {
        this.destination = destination;
    }

    public LocalDate getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(LocalDate dateDepart) {
        this.dateDepart = dateDepart;
    }

    public LocalDate getDateRetour() {
        return dateRetour;
    }

    public void setDateRetour(LocalDate dateRetour) {
        this.dateRetour = dateRetour;
    }

    public int getNombrePersonnes() {
        return nombrePersonnes;
    }

    public void setNombrePersonnes(int nombrePersonnes) {
        this.nombrePersonnes = nombrePersonnes;
    }

    public double getTotalAPayer() {
        return totalAPayer;
    }

    public void setTotalAPayer(double totalAPayer) {
        this.totalAPayer = totalAPayer;
    }

    @Override
    public String toString() {
        return "VoyageEntity{" +
                "id=" + id +
                ", client=" + client +
                ", destination=" + destination +
                ", dateDepart=" + dateDepart +
                ", dateRetour=" + dateRetour +
                ", nombrePersonnes=" + nombrePersonnes +
                ", totalAPayer=" + totalAPayer +
                '}';
    }
}
