package com.example.gestionagencevoyage.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "destination")
public class DestinationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String nom;
    @Column(nullable = false)
    private String description;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private PaysEnum pays;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private RegionEnum region;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TypeVoyageEnum typeVoyage;
    @Column(nullable = false)
    private LocalDate dateDebut;
    @Column(nullable = false)
    private LocalDate dateFin;
    @Column(nullable = false)
    private int capacite;
    @Column(nullable = false)
    private int placesDisponibles;
    @Column(nullable = false)
    private double prixParPersonne;
    @ElementCollection
    @CollectionTable(name = "destination_images", joinColumns = @JoinColumn(name = "destination_id"), foreignKey = @ForeignKey(name = "fk_destination"))
    @Column(name = "image", nullable = false)
    private List<String> images;

    public DestinationEntity() {
    }

    public DestinationEntity(String nom, String description, PaysEnum pays, RegionEnum region, TypeVoyageEnum typeVoyage, LocalDate dateDebut, LocalDate dateFin, int capacite, int placesDisponibles, double prixParPersonne, List<String> images) {
        this.nom = nom;
        this.description = description;
        this.pays = pays;
        this.region = region;
        this.typeVoyage = typeVoyage;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.capacite = capacite;
        this.placesDisponibles = placesDisponibles;
        this.prixParPersonne = prixParPersonne;
        this.images = images;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public PaysEnum getPays() {
        return pays;
    }

    public void setPays(PaysEnum pays) {
        this.pays = pays;
    }

    public RegionEnum getRegion() {
        return region;
    }

    public void setRegion(RegionEnum region) {
        this.region = region;
    }

    public TypeVoyageEnum getTypeVoyage() {
        return typeVoyage;
    }

    public void setTypeVoyage(TypeVoyageEnum typeVoyage) {
        this.typeVoyage = typeVoyage;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public int getPlacesDisponibles() {
        return placesDisponibles;
    }

    public void setPlacesDisponibles(int placesDisponibles) {
        this.placesDisponibles = placesDisponibles;
    }

    public double getPrixParPersonne() {
        return prixParPersonne;
    }

    public void setPrixParPersonne(double prixParPersonne) {
        this.prixParPersonne = prixParPersonne;
    }

    @Override
    public String toString() {
        return "DestinationEntity{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", description='" + description + '\'' +
                ", pays=" + pays +
                ", region=" + region +
                ", typeVoyage=" + typeVoyage +
                ", dateDebut=" + dateDebut +
                ", dateFin=" + dateFin +
                ", capacite=" + capacite +
                ", placesDisponibles=" + placesDisponibles +
                ", prixParPersonne=" + prixParPersonne +
                ", images=" + images +
                '}';
    }
}
