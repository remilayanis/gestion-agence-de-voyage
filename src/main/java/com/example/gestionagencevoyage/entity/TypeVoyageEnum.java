package com.example.gestionagencevoyage.entity;

public enum TypeVoyageEnum {
    AVENTURE,
    PLAGE,
    CULTURE,
    AFFAIRES,
    AUTRE;
}