package com.example.gestionagencevoyage.utils;

import com.example.gestionagencevoyage.GestionAgenceVoyageApp;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.util.Objects;

public class GestionVoyageUtils {

    public static Parent loadFxml(String fxmlPath) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(GestionVoyageUtils.class.getResource(fxmlPath)));
        fxmlLoader.setControllerFactory(GestionAgenceVoyageApp.getConfigurableApplicationContext()::getBean);
        return fxmlLoader.load();
    }
}
