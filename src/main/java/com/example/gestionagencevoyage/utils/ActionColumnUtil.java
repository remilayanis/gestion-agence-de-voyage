package com.example.gestionagencevoyage.utils;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableCell;
import javafx.scene.control.Button;
import javafx.scene.control.TableRow;
import javafx.scene.layout.HBox;

public class ActionColumnUtil<T> {

    public static <T> TableColumn<T, Void> create(String columnName, ActionCallback<T> callback) {
        TableColumn<T, Void> actionColumn = new TableColumn<>(columnName);
        actionColumn.setSortable(false);
        actionColumn.setPrefWidth(200);
        actionColumn.setCellFactory(param -> new TableCell<T, Void>() {
            final Button detailButton = new Button("Détail");
            final Button editButton = new Button("Modifier");
            final Button deleteButton = new Button("Supprimer");

            {
                detailButton.setOnAction(event -> callback.onDetail(getTableView().getItems().get(getIndex())));
                editButton.setOnAction(event -> callback.onEdit(getTableView().getItems().get(getIndex())));
                deleteButton.setOnAction(event -> callback.onDelete(getTableView().getItems().get(getIndex())));
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    HBox buttonBox = new HBox(detailButton, editButton, deleteButton);
                    buttonBox.setSpacing(5);
                    setGraphic(buttonBox);
                }
            }
        });

        return actionColumn;
    }

    public static <T> TableColumn<T, Void> createSelectOnly(String columnName, SelectCallback<T> callback) {
        TableColumn<T, Void> actionColumn = new TableColumn<>(columnName);
        actionColumn.setSortable(false);
        actionColumn.setPrefWidth(100);

        // Keep track of the currently selected row
        ObjectProperty<TableRow<T>> selectedRow = new SimpleObjectProperty<>(null);

        actionColumn.setCellFactory(param -> new TableCell<T, Void>() {
            final Button selectButton = new Button("Sélectionner");

            {
                selectButton.setOnAction(event -> {
                    T selectedItem = getTableView().getItems().get(getIndex());

                    // Clear style from the previously selected row
                    if (selectedRow.get() != null) {
                        selectedRow.get().setStyle("");
                    }

                    callback.onSelect(selectedItem);

                    // Add visual effect to the selected row
                    getTableRow().setStyle("-fx-background-color: lightblue;");

                    // Update the currently selected row
                    selectedRow.set(getTableRow());
                });
            }

            @Override
            protected void updateItem(Void item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    HBox buttonBox = new HBox(selectButton);
                    buttonBox.setSpacing(5);
                    setGraphic(buttonBox);
                }
            }
        });

        return actionColumn;
    }



    public interface ActionCallback<T> {
        void onDetail(T item);

        void onEdit(T item);

        void onDelete(T item);
    }

    public interface SelectCallback<T> {
        void onSelect(T item);
    }
}
