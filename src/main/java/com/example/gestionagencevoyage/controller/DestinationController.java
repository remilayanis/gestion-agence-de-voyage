package com.example.gestionagencevoyage.controller;

import com.example.gestionagencevoyage.entity.DestinationEntity;
import com.example.gestionagencevoyage.entity.PaysEnum;
import com.example.gestionagencevoyage.entity.TypeVoyageEnum;
import com.example.gestionagencevoyage.repository.DestinationRepository;
import com.example.gestionagencevoyage.utils.ActionColumnUtil;
import com.example.gestionagencevoyage.utils.GestionVoyageUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Controller class for managing destination-related actions in the application.
 */
@Component
public class DestinationController implements Initializable {


    @Autowired
    private DestinationRepository destinationRepository;
    private Stage destinationFormStage;
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    @FXML
    public TableView<DestinationEntity> destinationList;
    @FXML
    public Label formLabel;
    @FXML
    public TextField destinationNameTextField;
    @FXML
    public TextArea destinationDescriptionTextField;
    @FXML
    public ListView<String> destinationImagesListView;
    @FXML
    public Button destinationFormButton;
    @FXML
    private ImageView imageCarousel;
    @FXML
    public ChoiceBox<TypeVoyageEnum> destinationTypeChoiseBox;
    @FXML
    public ChoiceBox<PaysEnum> destinationPaysChoiseBox;
    @FXML
    public DatePicker destinationDateDebutDatePiker;
    @FXML
    public DatePicker destinationDateFinDatePiker;
    @FXML
    public TextField destinationCapaciteTextField;
    @FXML
    public TextField destinationPrixParPersonneTextField;

    @FXML
    public Label detailNomDestination;
    @FXML
    public Label detailDescriptionDestination;
    @FXML
    public Label detailTypeDestination;
    @FXML
    public Label detailPaysDestination;
    @FXML
    public Label detailDateDebutDestination;
    @FXML
    public Label detailDateFinDestination;
    @FXML
    public Label detailCapaciteDestination;
    @FXML
    public Label detailPlaceDispoDestination;
    @FXML
    public Label detailPrixDestination;
    @FXML
    public Button detailBtnFermer;


    private int currentImageIndex = 0;
    private String[] images;

    private boolean isConfigureActionColumn = false;
    private DestinationEntity destinationToEdit = null; // Track the destination to be edited.

    /**
     * Initializes the controller after its root element has been completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or null if the location is not known.
     * @param resources The resources used to localize the root object, or null if the root object was not localized.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (!isConfigureActionColumn) {
            configureActionColumn();
            isConfigureActionColumn = true;
        }
        loadAllDestinations();
    }

    /**
     * Opens the destination form for adding a new destination.
     *
     * @param actionEvent The event triggered by the user action.
     */
    @FXML
    public void openDestinationForm(ActionEvent actionEvent) {
        destinationToEdit = null;
        initForm("Ajouter", null);
    }

    /**
     * Uploads images for the destination from the user's file system.
     *
     * @param actionEvent The event triggered by the user action.
     */
    @FXML
    public void uploadImages(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Images");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif"));

        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(destinationImagesListView.getScene().getWindow());

        if (selectedFiles != null && !selectedFiles.isEmpty()) {
            ObservableList<String> imageNames = FXCollections.observableArrayList();
            for (File file : selectedFiles) {
                imageNames.add(file.getPath());
            }
            destinationImagesListView.setItems(imageNames);
            destinationImagesListView.setCellFactory(param -> new ImageListCell());
        }
    }

    /**
     * Handles the action performed when the destination form button is clicked.
     *
     * @param actionEvent The event triggered by the user action.
     */
    @FXML
    public void handleDestinationFormButton(ActionEvent actionEvent) {
        String missingFields = getMissingFields();
        if (missingFields.isEmpty()) {
            if (destinationToEdit != null) {
                updateDestination();
            } else {
                addDestination();
            }
        } else {
            showAlert(missingFields);
        }
    }

    /**
     * Initializes and displays the destination form.
     *
     * @param title       The title of the form.
     * @param destination The destination entity to be edited (null if adding a new destination).
     */
    private void initForm(String title, DestinationEntity destination) {
        try {
            // Load the destination form FXML file.
            Parent root = GestionVoyageUtils.loadFxml("/gestion_destination/formulaire_destination.fxml");

            // Create a new stage for the destination form.
            destinationFormStage = new Stage();
            destinationFormStage.initModality(Modality.WINDOW_MODAL);
            destinationFormStage.initOwner(destinationList.getScene().getWindow());

            // Set the stage title and form label.
            destinationFormStage.setTitle(title + " une Destination");
            formLabel.setText(title + " une Destination");
            destinationFormButton.setText(title);

            // Get the values of the PaysEnum and convert them to an observable list
            ObservableList<PaysEnum> paysList = FXCollections.observableArrayList(PaysEnum.values());
            // Set the items for the ChoiceBox
            destinationPaysChoiseBox.setItems(paysList);
            // Get the values of the PaysEnum and convert them to an observable list
            ObservableList<TypeVoyageEnum> typeVoyageList = FXCollections.observableArrayList(TypeVoyageEnum.values());
            // Set the items for the ChoiceBox
            destinationTypeChoiseBox.setItems(typeVoyageList);

            if (destination != null) {
                // If updating, populate the form with the destination's data.
                destinationToEdit = destination;
                fillDestinationFormFields(destination);
            } else {
                // If adding, clear the form fields.
                clearFormFields();
            }

            // Set the scene and show the stage.
            destinationFormStage.setScene(new Scene(root));
            destinationFormStage.showAndWait();
            loadAllDestinations();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fills the destination form fields with data from an existing destination.
     *
     * @param destination The destination entity to be edited.
     */
    private void fillDestinationFormFields(DestinationEntity destination) {
        destinationNameTextField.setText(destination.getNom());
        destinationDescriptionTextField.setText(destination.getDescription());
        destinationTypeChoiseBox.setValue(destination.getTypeVoyage());
        destinationPaysChoiseBox.setValue(destination.getPays());
        destinationDateDebutDatePiker.setValue(destination.getDateDebut());
        destinationDateFinDatePiker.setValue(destination.getDateFin());
        destinationCapaciteTextField.setText(String.valueOf(destination.getCapacite()));
        destinationPrixParPersonneTextField.setText(String.valueOf(destination.getPrixParPersonne()));
        // Populate the image list view.
        ObservableList<String> images = FXCollections.observableArrayList(destination.getImages());
        destinationImagesListView.setItems(images);
        destinationImagesListView.setCellFactory(param -> new ImageListCell());
    }

    /**
     * Clears all form fields in the destination form.
     */
    private void clearFormFields() {
        destinationNameTextField.clear();
        destinationDescriptionTextField.clear();
        destinationTypeChoiseBox.setValue(null);
        destinationPaysChoiseBox.setValue(null);
        destinationDateDebutDatePiker.setValue(null);
        destinationDateFinDatePiker.setValue(null);
        destinationCapaciteTextField.clear();
        destinationPrixParPersonneTextField.clear();
        destinationImagesListView.getItems().clear();
    }

    /**
     * Adds a new destination to the database based on the form input.
     */
    private void addDestination() {
        // Get values from form fields.
        String name = destinationNameTextField.getText();
        String description = destinationDescriptionTextField.getText();
        TypeVoyageEnum typeVoyage = destinationTypeChoiseBox.getValue();
        PaysEnum pays = destinationPaysChoiseBox.getValue();
        LocalDate dateDebut = destinationDateDebutDatePiker.getValue();
        LocalDate dateFin = destinationDateFinDatePiker.getValue();
        int capacite = Integer.parseInt(destinationCapaciteTextField.getText());
        double prix = Double.parseDouble(destinationPrixParPersonneTextField.getText());
        List<String> images = destinationImagesListView.getItems();

        // Create a new destination entity and save it to the database.
        DestinationEntity newDestination = new DestinationEntity(name, description, pays,
                pays.getRegion(), typeVoyage, dateDebut, dateFin, capacite, capacite,
                prix, images);
        destinationRepository.save(newDestination);

        // Clear form fields and close the stage.
        clearFormFields();
        destinationFormStage.close();
    }

    /**
     * Updates an existing destination in the database based on the form input.
     */
    private void updateDestination() {
        if (destinationToEdit != null) {
            // Get values from form fields.
            String name = destinationNameTextField.getText();
            String description = destinationDescriptionTextField.getText();
            TypeVoyageEnum typeVoyage = destinationTypeChoiseBox.getValue();
            PaysEnum pays = destinationPaysChoiseBox.getValue();
            LocalDate dateDebut = destinationDateDebutDatePiker.getValue();
            LocalDate dateFin = destinationDateFinDatePiker.getValue();
            int capacite = Integer.parseInt(destinationCapaciteTextField.getText());
            double prix = Double.parseDouble(destinationPrixParPersonneTextField.getText());
            List<String> images = destinationImagesListView.getItems();

            // Update the destination entity with new values and save it to the database.
            destinationToEdit.setNom(name);
            destinationToEdit.setDescription(description);
            destinationToEdit.setPays(pays);
            destinationToEdit.setRegion(pays.getRegion());
            destinationToEdit.setTypeVoyage(typeVoyage);
            destinationToEdit.setDateDebut(dateDebut);
            destinationToEdit.setDateFin(dateFin);
            destinationToEdit.setCapacite(capacite);
            destinationToEdit.setPrixParPersonne(prix);
            destinationToEdit.setImages(images);

            destinationRepository.save(destinationToEdit);

            // Clear form fields and close the stage.
            clearFormFields();
            destinationFormStage.close();
            destinationToEdit = null; // Reset the destination to edit.
        }
    }

    /**
     * Configures the action column in the destination list view.
     */
    private void configureActionColumn() {
        // Create and configure an action column with callbacks.
        TableColumn<DestinationEntity, Void> actionColumn = ActionColumnUtil.create("Action", new ActionColumnUtil.ActionCallback<DestinationEntity>() {
            @Override
            public void onDetail(DestinationEntity item) {
                viewDestinationDetails(item);
            }

            @Override
            public void onEdit(DestinationEntity item) {
                initForm("Modifier", item);
            }

            @Override
            public void onDelete(DestinationEntity item) {
                deleteDestination(item);
            }
        });

        destinationList.getColumns().add(actionColumn);
    }

    /**
     * Custom list cell for displaying images in the destination form.
     */
    private class ImageListCell extends ListCell<String> {
        private final Button removeButton = new Button("X");

        public ImageListCell() {
            removeButton.setOnAction(event -> {
                getListView().getItems().remove(getItem());
            });
        }

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);

            if (empty || item == null) {
                setGraphic(null);
            } else {
                HBox cellBox = new HBox();
                cellBox.getChildren().addAll(removeButton, new Button(item));
                setGraphic(cellBox);
            }
        }
    }

    /**
     * Deletes a destination from the database.
     *
     * @param destinationEntity The destination entity to be deleted.
     */
    private void deleteDestination(DestinationEntity destinationEntity) {
        destinationRepository.delete(destinationEntity);
        loadAllDestinations();
    }

    /**
     * Loads all destinations from the database and updates the destination list view.
     */
    private void loadAllDestinations() {
        List<DestinationEntity> destinations = destinationRepository.findAll();
        destinationList.getItems().setAll(destinations);
    }

    /**
     * Displays detailed information about a destination in a separate stage.
     *
     * @param destinationEntity The destination entity to view details for.
     */
    public void viewDestinationDetails(DestinationEntity destinationEntity) {
        try {
            // Load the destination image carousel FXML file.
            Parent root = GestionVoyageUtils.loadFxml("/gestion_destination/detail_destination.fxml");

            // Create a new stage for the image carousel.
            Stage detailDestination = new Stage();
            detailDestination.initModality(Modality.WINDOW_MODAL);
            detailDestination.initOwner(destinationList.getScene().getWindow());

            // Set the stage title.
            detailDestination.setTitle("Détail de la destination");
            detailNomDestination.setText(destinationEntity.getNom());
            detailDescriptionDestination.setText(destinationEntity.getDescription());
            detailTypeDestination.setText(destinationEntity.getTypeVoyage().name());
            detailPaysDestination.setText(destinationEntity.getPays().getNom() + " | " + destinationEntity.getPays().getRegion().getDisplayName());
            detailDateDebutDestination.setText(destinationEntity.getDateDebut().format(formatter));
            detailDateFinDestination.setText(destinationEntity.getDateFin().format(formatter));
            detailCapaciteDestination.setText(String.valueOf(destinationEntity.getCapacite()));
            detailPlaceDispoDestination.setText(String.valueOf(destinationEntity.getPlacesDisponibles()));
            detailPrixDestination.setText(String.valueOf(destinationEntity.getPrixParPersonne()));
            detailBtnFermer.setOnAction(event -> detailDestination.close());
            // Get the controller and pass the destination images to it.
            String[] destinationImages = destinationEntity.getImages().toArray(new String[0]);
            initialize(destinationImages);
            detailDestination.setOnCloseRequest(event -> currentImageIndex = 0);
            // Set the scene and show the stage.
            detailDestination.setScene(new Scene(root));
            detailDestination.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Initializes the image carousel with an array of image file paths.
     *
     * @param images The array of image file paths.
     */
    public void initialize(String[] images) {
        this.images = images;
        currentImageIndex = 0;
        showImage(currentImageIndex);
    }

    /**
     * Shows the next image in the image carousel.
     */
    @FXML
    public void showNextImage() {
        currentImageIndex = (currentImageIndex + 1) % images.length;
        showImage(currentImageIndex);
    }

    /**
     * Shows the previous image in the image carousel.
     */
    @FXML
    public void showPreviousImage() {
        currentImageIndex = (currentImageIndex - 1 + images.length) % images.length;
        showImage(currentImageIndex);
    }

    /**
     * Displays an image at the specified index in the image carousel.
     *
     * @param index The index of the image to display.
     */
    private void showImage(int index) {
        if (images != null && images.length > 0) {
            Image image = new Image("file:" + images[index]); // Load image from file
            imageCarousel.setImage(image);
        }
    }

    /**
     * Displays a warning alert for missing required fields in the destination form.
     *
     * @param missingFields A string containing the names of missing fields.
     */
    private void showAlert(String missingFields) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Champs obligatoires manquants");
        alert.setHeaderText(null);
        alert.setContentText("Veuillez remplir les champs obligatoires suivants:\n" + missingFields);
        alert.showAndWait();
    }

    /**
     * Generates a string listing the names of missing required fields in the destination form.
     *
     * @return A string with the names of missing fields.
     */
    private String getMissingFields() {
        StringBuilder missingFields = new StringBuilder();

        if (destinationNameTextField.getText().isEmpty()) {
            missingFields.append("  - Nom de la destination\n");
        }

        if (destinationDescriptionTextField.getText().isEmpty()) {
            missingFields.append("  - Description de la destination\n");
        }

        if (destinationTypeChoiseBox.getValue() == null) {
            missingFields.append("  - Type de voyage\n");
        }

        if (destinationPaysChoiseBox.getValue() == null) {
            missingFields.append("  - Pays de destination\n");
        }

        if (destinationDateDebutDatePiker.getValue() == null) {
            missingFields.append("  - Date de début\n");
        }

        if (destinationDateFinDatePiker.getValue() == null) {
            missingFields.append("  - Date de fin\n");
        }

        if (destinationDateDebutDatePiker.getValue() != null && destinationDateFinDatePiker.getValue() != null) {
            LocalDate debutDate = destinationDateDebutDatePiker.getValue();
            LocalDate finDate = destinationDateFinDatePiker.getValue();

            if (finDate.isBefore(debutDate)) {
                missingFields.append("  - La date de fin doit être postérieure à la date de début\n");
            }
        }

        if (destinationCapaciteTextField.getText().isEmpty()) {
            missingFields.append("  - Capacité\n");
        } else {
            try {
                int capacite = Integer.parseInt(destinationCapaciteTextField.getText());
                if (capacite <= 0) {
                    missingFields.append("  - Capacité doit être supérieure à zéro\n");
                }
            } catch (NumberFormatException e) {
                missingFields.append("  - Capacité doit être un nombre entier\n");
            }
        }

        if (destinationPrixParPersonneTextField.getText().isEmpty()) {
            missingFields.append("  - Prix par personne\n");
        } else {
            try {
                double prix = Double.parseDouble(destinationPrixParPersonneTextField.getText());
                if (prix <= 0) {
                    missingFields.append("  - Prix doit être supérieur à zéro\n");
                }
            } catch (NumberFormatException e) {
                missingFields.append("  - Prix doit être un nombre\n");
            }
        }

        if (destinationImagesListView.getItems().isEmpty()) {
            missingFields.append("  - Images de la destination\n");
        }

        return missingFields.toString().trim();
    }

}
