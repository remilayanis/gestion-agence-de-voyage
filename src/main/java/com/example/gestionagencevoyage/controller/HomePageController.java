package com.example.gestionagencevoyage.controller;

import com.example.gestionagencevoyage.GestionAgenceVoyageApp;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Objects;

@Component
public class HomePageController {


    public static void loadView(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(HomePageController.class.getResource("/auth.fxml")));
        fxmlLoader.setControllerFactory(GestionAgenceVoyageApp.getConfigurableApplicationContext()::getBean);
        Parent view = fxmlLoader.load();
        stage.setScene(new Scene(view));
        stage.show();
    }

}
