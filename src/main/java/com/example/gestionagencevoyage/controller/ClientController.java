package com.example.gestionagencevoyage.controller;

import com.example.gestionagencevoyage.entity.ClientEntity;
import com.example.gestionagencevoyage.repository.ClientRepository;
import com.example.gestionagencevoyage.utils.ActionColumnUtil;
import com.example.gestionagencevoyage.utils.GestionVoyageUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Component
public class ClientController implements Initializable {
    @Autowired
    private ClientRepository clientRepository;

    private ClientEntity clientToEdit;
    private Stage clientFormStage;
    private boolean isConfigureActionColumn = false;

    @FXML
    private TableView<ClientEntity> clientList;
    @FXML
    private TextField clientNameTextField;
    @FXML
    private TextField clientSurnameTextField;
    @FXML
    private TextField clientAddressTextField;
    @FXML
    private TextField clientPhoneNumberTextField;
    @FXML
    private TextField clientEmailTextField;
    @FXML
    private Button clientFormButton;
    @FXML
    private Label formLabel;
    @FXML
    private Text detailNom;
    @FXML
    private Text detailPrenom;
    @FXML
    private Text detailAdresse;
    @FXML
    private Text detailTelephone;
    @FXML
    private Text detailEmail;
    @FXML
    private Button detailBtnFermer;

    /**
     * Initializes the controller.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize the controller and configure action column.
        if (!isConfigureActionColumn) {
            configureActionColumn();
            isConfigureActionColumn = true;
        }
        loadAllClients();
    }

    /**
     * Opens the form to add a new client.
     */
    @FXML
    public void openAddClientForm() {
        clientToEdit = null;
        showClientForm("Ajouter", null);
    }

    /**
     * Opens the form to edit an existing client.
     *
     * @param client The client to be edited.
     */
    public void openEditClientForm(ClientEntity client) {
        clientToEdit = client;
        showClientForm("Modifier", client);
    }

    /**
     * Shows the client form for adding or editing a client.
     *
     * @param title  The title for the form (e.g., "Ajouter" or "Modifier").
     * @param client The client entity to edit (null for adding a new client).
     */
    private void showClientForm(String title, ClientEntity client) {
        try {
            // Load the client form FXML file.
            Parent root = GestionVoyageUtils.loadFxml("/gestion_client/formulaire_client.fxml");

            // Create a new stage for the client form.
            clientFormStage = new Stage();
            clientFormStage.initModality(Modality.WINDOW_MODAL);
            clientFormStage.initOwner(clientList.getScene().getWindow());

            // Set the stage title and form label.
            clientFormStage.setTitle(title + " un Client");
            formLabel.setText(title + " un Client");
            clientFormButton.setText(title);

            if (client != null) {
                fillClientFormFields(client);
            }

            // Define the action to be taken when the form button is clicked.
            clientFormButton.setOnAction(event -> {
                if (clientToEdit != null) {
                    updateClient();
                } else {
                    addClient();
                }
            });

            // Set the scene and show the stage.
            clientFormStage.setScene(new Scene(root));
            clientFormStage.showAndWait();
            loadAllClients();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Fills the client form fields with data from an existing client.
     *
     * @param client The client entity whose data is to be displayed.
     */
    private void fillClientFormFields(ClientEntity client) {
        clientNameTextField.setText(client.getNom());
        clientSurnameTextField.setText(client.getPrenom());
        clientAddressTextField.setText(client.getAdresse());
        clientPhoneNumberTextField.setText(client.getNumeroTelephone());
        clientEmailTextField.setText(client.getEmail());
    }

    /**
     * Adds a new client to the database.
     */
    public void addClient() {
        // Get values from form fields.
        String nomValue = clientNameTextField.getText();
        String prenomValue = clientSurnameTextField.getText();
        String adresseValue = clientAddressTextField.getText();
        String telephoneValue = clientPhoneNumberTextField.getText();
        String emailValue = clientEmailTextField.getText();

        // Check if required fields are empty
        String missingFields = checkRequiredFields(nomValue, prenomValue, adresseValue, telephoneValue, emailValue);

        if (!missingFields.isEmpty()) {
            showEmptyFieldsAlert(missingFields);
            return;
        }

        // Create a new client entity and save it to the database.
        ClientEntity newClient = new ClientEntity(nomValue, prenomValue, adresseValue, telephoneValue, emailValue);
        clientRepository.save(newClient);

        // Clear form fields and close the stage.
        clearFormFields();
        clientFormStage.close();
    }

    /**
     * Updates an existing client in the database.
     */
    public void updateClient() {
        if (clientToEdit != null) {
            // Get values from form fields.
            String nomValue = clientNameTextField.getText();
            String prenomValue = clientSurnameTextField.getText();
            String adresseValue = clientAddressTextField.getText();
            String telephoneValue = clientPhoneNumberTextField.getText();
            String emailValue = clientEmailTextField.getText();

            // Check if required fields are empty
            String missingFields = checkRequiredFields(nomValue, prenomValue, adresseValue, telephoneValue, emailValue);

            if (!missingFields.isEmpty()) {
                showEmptyFieldsAlert(missingFields);
                return;
            }

            // Update the client entity with new values and save it to the database.
            clientToEdit.setNom(nomValue);
            clientToEdit.setPrenom(prenomValue);
            clientToEdit.setAdresse(adresseValue);
            clientToEdit.setNumeroTelephone(telephoneValue);
            clientToEdit.setEmail(emailValue);

            clientRepository.save(clientToEdit);
            clientToEdit = null;

            // Clear form fields and close the stage.
            clearFormFields();
            clientFormStage.close();
        }
    }

    /**
     * Clears the form fields.
     */
    private void clearFormFields() {
        clientNameTextField.clear();
        clientSurnameTextField.clear();
        clientAddressTextField.clear();
        clientPhoneNumberTextField.clear();
        clientEmailTextField.clear();
    }

    /**
     * Configures the action column for the client list.
     */
    private void configureActionColumn() {
        // Create and configure an action column with callbacks for detail, edit, and delete actions.
        TableColumn<ClientEntity, Void> actionColumn = ActionColumnUtil.create("Action", new ActionColumnUtil.ActionCallback<ClientEntity>() {
            @Override
            public void onDetail(ClientEntity item) {
                viewClientDetails(item);
            }

            @Override
            public void onEdit(ClientEntity item) {
                openEditClientForm(item);
            }

            @Override
            public void onDelete(ClientEntity item) {
                deleteClient(item);
            }
        });

        clientList.getColumns().add(actionColumn);
    }

    /**
     * Opens a detailed view of a client's information.
     *
     * @param client The client entity for which to display details.
     */
    private void viewClientDetails(ClientEntity client) {
        try {
            // Load the detailed client view FXML file.
            Parent root = GestionVoyageUtils.loadFxml("/gestion_client/detail_client.fxml");

            // Create a new stage for displaying client details.
            Stage detailsStage = new Stage();
            detailsStage.initModality(Modality.WINDOW_MODAL);
            detailsStage.initOwner(clientList.getScene().getWindow());
            detailsStage.setTitle("Détails du Client");

            // Set the details in the view.
            detailNom.setText(client.getNom());
            detailPrenom.setText(client.getPrenom());
            detailAdresse.setText(client.getAdresse());
            detailTelephone.setText(client.getNumeroTelephone());
            detailEmail.setText(client.getEmail());

            // Define the action to close the details stage.
            detailBtnFermer.setOnAction(event -> detailsStage.close());
            detailsStage.setScene(new Scene(root));
            detailsStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes a client from the database.
     *
     * @param client The client entity to be deleted.
     */
    private void deleteClient(ClientEntity client) {
        // Delete the client entity from the repository and reload the client list.
        clientRepository.delete(client);
        loadAllClients();
    }

    /**
     * Loads all clients from the repository and populates the client list.
     */
    private void loadAllClients() {
        // Retrieve all clients from the repository and populate the client list.
        List<ClientEntity> clients = clientRepository.findAll();
        clientList.getItems().setAll(clients);
    }

    /**
     * Checks if required fields are empty and if they have the correct format.
     */
    private String checkRequiredFields(String nom, String prenom, String adresse, String telephone, String email) {
        String missingFields = "";

        if (nom.isEmpty()) {
            missingFields += "- Nom\n";
        }
        if (prenom.isEmpty()) {
            missingFields += "- Prenom\n";
        }
        if (adresse.isEmpty()) {
            missingFields += "- Adresse\n";
        }
        if (telephone.isEmpty()) {
            missingFields += "- Telephone (champ requis)\n";
        } else if (!isValidPhoneNumber(telephone)) {
            missingFields += "- Telephone (format invalide)\n";
        }
        if (email.isEmpty()) {
            missingFields += "- Email (champ requis)\n";
        } else if (!isValidEmail(email)) {
            missingFields += "- Email (format invalide)\n";
        }

        return missingFields;
    }

    /**
     * Checks if the phone number has a valid format.
     */
    private boolean isValidPhoneNumber(String phoneNumber) {
        // Implement your validation logic here.
        // For simplicity, let's assume a valid phone number is numeric and has a certain length.
        return phoneNumber.matches("\\d{10}");
    }

    /**
     * Checks if the email has a valid format using regex.
     */
    private boolean isValidEmail(String email) {
        // Define a regular expression for a basic email format check.
        // This is a simplified example and may not cover all valid email formats.
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

        // Use Pattern and Matcher classes to perform the regex check.
        return email.matches(emailRegex);
    }

    /**
     * Shows an alert when required fields are empty.
     */
    private void showEmptyFieldsAlert(String missingFields) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Champs requis");
        alert.setHeaderText("Veuillez remplir tous les champs requis:");
        alert.setContentText(missingFields);
        alert.showAndWait();
    }
}