package com.example.gestionagencevoyage.controller;

import com.example.gestionagencevoyage.entity.ClientEntity;
import com.example.gestionagencevoyage.entity.DestinationEntity;
import com.example.gestionagencevoyage.entity.VoyageEntity;
import com.example.gestionagencevoyage.repository.ClientRepository;
import com.example.gestionagencevoyage.repository.DestinationRepository;
import com.example.gestionagencevoyage.repository.VoyageRepository;
import com.example.gestionagencevoyage.utils.ActionColumnUtil;
import com.example.gestionagencevoyage.utils.GestionVoyageUtils;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

@Component
public class VoyageController implements Initializable {

    @Autowired
    private VoyageRepository voyageRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private DestinationRepository destinationRepository;
    private ClientEntity selectedClient = null;
    private DestinationEntity selectedDestination = null;
    private boolean isConfigureActionColumn = false;

    @FXML
    public TableView<DestinationEntity> destinationList;
    @FXML
    private TableView<ClientEntity> clientList;
    @FXML
    public TableView<VoyageEntity> voyageTable;
    @FXML
    private TableColumn<VoyageEntity, String> clientColumn;
    @FXML
    private TableColumn<VoyageEntity, String> destinationColumn;
    @FXML
    public DatePicker dateDepart;
    @FXML
    public DatePicker dateRetour;
    @FXML
    public TextField nbrPersonne;


    private Stage voyageFormStage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (clientList != null) {
            configureSelectColumnClientList();
            loadAllClients();
        }
        if (destinationList != null) {
            configureSelectColumnDestinationList();
            loadAllDestinations();
        }
        if (!isConfigureActionColumn) {
            configureActionColumn();
            isConfigureActionColumn = true;
        }
        loadAllVoyages();
    }

    /**
     * Configures the action column for the Voyage list.
     */
    private void configureActionColumn() {
        // Create and configure an action column with callbacks for detail, edit, and delete actions.
        TableColumn<VoyageEntity, Void> actionColumn = ActionColumnUtil.create("Action", new ActionColumnUtil.ActionCallback<VoyageEntity>() {
            @Override
            public void onDetail(VoyageEntity item) {
                viewVoyageDetails(item);
            }

            @Override
            public void onEdit(VoyageEntity item) {
                openEditVoyageForm(item);
            }

            @Override
            public void onDelete(VoyageEntity item) {
                deleteVoyage(item);
            }
        });

        voyageTable.getColumns().add(actionColumn);
    }

    private void deleteVoyage(VoyageEntity item) {
        voyageRepository.delete(item);
        loadAllVoyages();
    }

    private void openEditVoyageForm(VoyageEntity item) {
    }

    private void viewVoyageDetails(VoyageEntity item) {
    }

    private void loadAllVoyages() {
        List<VoyageEntity> voyages = voyageRepository.findAll();
        voyageTable.getItems().setAll(voyages);
        clientColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getClient().getNom()));
        destinationColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDestination().getNom()));
    }

    public void ajouterVoyage(ActionEvent actionEvent) throws IOException {
        Parent root = GestionVoyageUtils.loadFxml("/gestion_voyage/formulaire_voyage.fxml");

        // Create a new stage for the destination form.
        voyageFormStage = new Stage();
        voyageFormStage.initModality(Modality.WINDOW_MODAL);

        voyageFormStage.setScene(new Scene(root));
        voyageFormStage.showAndWait();
    }

    public void addTrip(ActionEvent actionEvent) {

        if (!validateTrip()) {
            return;
        }

        // Retrieve the selected dates, number of persons, and total amount to pay from the UI elements
        LocalDate departureDate = dateDepart.getValue();
        LocalDate returnDate = dateRetour.getValue();
        int numberOfPersons = Integer.parseInt(nbrPersonne.getText());


        // Update placesDisponibles in the selected destination
        selectedDestination.setPlacesDisponibles(selectedDestination.getPlacesDisponibles() - numberOfPersons);
        destinationRepository.save(selectedDestination);

        // Create a new VoyageEntity and set its properties
        VoyageEntity newVoyage = new VoyageEntity();
        newVoyage.setClient(selectedClient);
        newVoyage.setDestination(selectedDestination);
        newVoyage.setDateDepart(departureDate);
        newVoyage.setDateRetour(returnDate);
        newVoyage.setNombrePersonnes(numberOfPersons);
        newVoyage.setTotalAPayer(numberOfPersons * selectedDestination.getPrixParPersonne());

        // Save the new voyage to the database using the repository
        voyageRepository.save(newVoyage);

        selectedClient = null;
        selectedDestination = null;

        // Close the voyage form stage
        if (voyageFormStage != null) {
            voyageFormStage.close();
        }
        loadAllVoyages();
    }

    private void configureSelectColumnClientList() {
        // Create and configure a select column with a callback for the select action.
        TableColumn<ClientEntity, Void> selectColumn = ActionColumnUtil.createSelectOnly("Action", new ActionColumnUtil.SelectCallback<ClientEntity>() {
            @Override
            public void onSelect(ClientEntity item) {
                handleSelectClient(item);
            }
        });

        clientList.getColumns().add(selectColumn);
    }

    private void configureSelectColumnDestinationList() {
        // Create and configure a select column with a callback for the select action.
        TableColumn<DestinationEntity, Void> selectColumn = ActionColumnUtil.createSelectOnly("Action", new ActionColumnUtil.SelectCallback<DestinationEntity>() {
            @Override
            public void onSelect(DestinationEntity item) {
                handleSelectDestination(item);
            }
        });

        destinationList.getColumns().add(selectColumn);
    }

    /**
     * Loads all clients from the repository and populates the client list.
     */
    private void loadAllClients() {
        // Retrieve all clients from the repository and populate the client list.
        List<ClientEntity> clients = clientRepository.findAll();
        clientList.getItems().setAll(clients);
    }

    /**
     * Loads all destinations from the database and updates the destination list view.
     */
    private void loadAllDestinations() {
        List<DestinationEntity> destinations = destinationRepository.findAll();
        destinationList.getItems().setAll(destinations);
    }

    private void handleSelectClient(ClientEntity item) {
        selectedClient = item;
    }

    private void handleSelectDestination(DestinationEntity item) {
        selectedDestination = item;
    }

    private boolean validateTrip() {
        // Vérifier si le client et la destination sont sélectionnés
        if (selectedClient == null || selectedDestination == null) {
            showAlert("Erreur", "Veuillez sélectionner à la fois un client et une destination.");
            return false;
        }

        // Vérifier si la date de départ est renseignée
        LocalDate departureDate = dateDepart.getValue();
        if (departureDate == null) {
            showAlert("Erreur de date", "Veuillez renseigner la date de départ.");
            return false;
        }

        // Vérifier si la date de retour est renseignée
        LocalDate returnDate = dateRetour.getValue();
        if (returnDate == null) {
            showAlert("Erreur de date", "Veuillez renseigner la date de retour.");
            return false;
        }

        // Vérifier si le nombre de personnes est renseigné
        String numberOfPersonsText = nbrPersonne.getText();
        if (numberOfPersonsText == null || numberOfPersonsText.trim().isEmpty()) {
            showAlert("Erreur de saisie", "Veuillez renseigner le nombre de personnes.");
            return false;
        }

        // Vérifier le format du nombre de personnes
        int numberOfPersons;
        try {
            numberOfPersons = Integer.parseInt(numberOfPersonsText);
        } catch (NumberFormatException e) {
            showAlert("Erreur de saisie", "Veuillez entrer un nombre valide pour le nombre de personnes.");
            return false;
        }

        // Vérifier si le nombre de personnes est supérieur à zéro
        if (numberOfPersons <= 0) {
            showAlert("Erreur de saisie", "Le nombre de personnes doit être supérieur à zéro.");
            return false;
        }

        // Vérifier la date de départ par rapport à la date de début de la destination
        if (departureDate.isBefore(selectedDestination.getDateDebut())) {
            showAlert("Erreur de date", "La date de départ doit être après ou égale à la date de début de la destination (" +
                    selectedDestination.getDateDebut() + ").");
            return false;
        }

        // Vérifier la date de retour par rapport à la date de fin de la destination
        if (returnDate.isAfter(selectedDestination.getDateFin())) {
            showAlert("Erreur de date", "La date de retour doit être avant ou égale à la date de fin de la destination (" +
                    selectedDestination.getDateFin() + ").");
            return false;
        }

        // Vérifier le nombre de personnes par rapport aux places disponibles de la destination
        if (numberOfPersons > selectedDestination.getPlacesDisponibles()) {
            showAlert("Erreur de capacité", "Le nombre de personnes ne peut pas dépasser les places disponibles de la destination (" +
                    selectedDestination.getPlacesDisponibles() + ").");
            return false;
        }

        // Return true if all validations pass
        return true;
    }

    private void showAlert(String title, String contentText) {
        // Créer et configurer une alerte
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(contentText);

        // Afficher l'alerte
        alert.showAndWait();
    }


}
