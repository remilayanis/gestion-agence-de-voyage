package com.example.gestionagencevoyage.controller;

import com.example.gestionagencevoyage.entity.EmployerEntity;
import com.example.gestionagencevoyage.repository.EmployerRepository;
import com.example.gestionagencevoyage.utils.GestionVoyageUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Controller
public class AuthController {

    @Autowired
    private EmployerRepository employerRepository;
    @FXML
    private TextField usernameField;

    @FXML
    private PasswordField passwordField;
    private Stage authStage;

    @FXML
    private void loginButtonAction(ActionEvent event) throws IOException {
        String username = usernameField.getText();
        String password = passwordField.getText();

        if (isAuthenticate(username, password)) {
            Parent root = GestionVoyageUtils.loadFxml("/home_page.fxml");
            authStage = new Stage();
            authStage.initModality(Modality.WINDOW_MODAL);
            authStage.setScene(new Scene(root));

            // Close the current stage
            Stage currentStage = (Stage) usernameField.getScene().getWindow();
            currentStage.close();

            // Show the new stage
            authStage.show();
        } else {
            showAlert("Authentication failed", "Vous n'êtes pas autorisé.");
        }
    }

    private boolean isAuthenticate(String username, String password) {
        EmployerEntity result = employerRepository.findByUsernameAndPassword(username, password);
        return result != null;
    }

    private void showAlert(String title, String contentText) {
        // Créer et configurer une alerte
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(contentText);

        // Afficher l'alerte
        alert.showAndWait();
    }
}
