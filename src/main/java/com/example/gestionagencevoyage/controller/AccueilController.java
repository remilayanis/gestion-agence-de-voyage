package com.example.gestionagencevoyage.controller;

import com.example.gestionagencevoyage.entity.DestinationEntity;
import com.example.gestionagencevoyage.entity.PaysEnum;
import com.example.gestionagencevoyage.entity.RegionEnum;
import com.example.gestionagencevoyage.entity.TypeVoyageEnum;
import com.example.gestionagencevoyage.repository.DestinationRepository;
import com.example.gestionagencevoyage.repository.DestinationSpecifications;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Component
public class AccueilController implements Initializable {

    @Autowired
    private DestinationRepository destinationRepository;
    @Autowired
    private DestinationController destinationController;
    @FXML
    private VBox destinationCardsContainer;
    @FXML
    public ChoiceBox<TypeVoyageEnum> destinationTypeChoiseBox;
    @FXML
    public ChoiceBox<PaysEnum> destinationPaysChoiseBox;
    @FXML
    public ChoiceBox<RegionEnum> destinationRegionChoiseBox;
    @FXML
    public DatePicker destinationDateDebutDatePicker;
    @FXML
    public DatePicker destinationDateFinDatePicker;
    @FXML
    public TextField destinationPlaceDispoTextField;
    @FXML
    public TextField destinationPrixParPersonneTextField;

    private ObservableList<PaysEnum> paysList;
    private ObservableList<TypeVoyageEnum> typeVoyageList;
    private ObservableList<RegionEnum> regionList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Fetch the list of destinations
        List<DestinationEntity> destinations = destinationRepository.findAll();

        // Initialize observable lists
        paysList = FXCollections.observableArrayList(PaysEnum.values());
        typeVoyageList = FXCollections.observableArrayList(TypeVoyageEnum.values());
        regionList = FXCollections.observableArrayList(RegionEnum.values());

        // Set the items for the ChoiceBox
        destinationPaysChoiseBox.setItems(paysList);
        destinationTypeChoiseBox.setItems(typeVoyageList);
        destinationRegionChoiseBox.setItems(regionList);

        // Generate and add destination cards to the VBox
        getCard(destinations);
    }

    private void getCard(List<DestinationEntity> destinations) {
        destinationCardsContainer.setSpacing(10);

        destinations.forEach(destination -> {
            HBox destinationCard = createDestinationCard(destination);
            destinationCard.setFocusTraversable(true);
            destinationCardsContainer.getChildren().add(destinationCard);
        });


    }

    private HBox createDestinationCard(DestinationEntity destination) {
        // Create an HBox for the destination card
        HBox card = new HBox(20); // Adjust spacing as needed

        // Create an ImageView for the destination image
        String imagePath = destination.getImages().get(0);
        ImageView imageView = new ImageView(new Image("file:" + imagePath)); // Assuming imagePath is the path to the image
        imageView.setFitWidth(150); // Adjust width as needed
        imageView.setFitHeight(150); // Adjust height as needed

        // Create a VBox for the title and description
        VBox detailsBox = new VBox(10); // Adjust spacing as needed

        // Create Labels for title and description
        Label titleLabel = new Label(destination.getNom());
        titleLabel.setStyle("-fx-font-weight: bold; -fx-font-size: 18;");
        Label descriptionLabel = new Label(destination.getDescription());
        // Create Labels for other details
        Label countryLabel = new Label("Pays: " + destination.getPays() + " | " + "Région: " + destination.getRegion());
        Label datesLabel = new Label("Dates du: " + destination.getDateDebut() + " au " + destination.getDateFin());
        Label capacityLabel = new Label("Capacité: " + destination.getCapacite() + " | " + "Places disponibles: " + destination.getPlacesDisponibles());
        Label priceLabel = new Label("Prix par personne: " + destination.getPrixParPersonne() + " $");

        // Create a Button
        Button detailsButton = new Button("Details");
        detailsButton.setOnAction(event -> destinationController.viewDestinationDetails(destination)); // Set the method to be called

        // Add labels and button to the VBox
        detailsBox.getChildren().addAll(titleLabel, descriptionLabel, countryLabel, datesLabel, capacityLabel, priceLabel, detailsButton);


        // Add ImageView and VBox to the HBox
        card.getChildren().addAll(imageView, detailsBox);

        // Set padding between each card
        card.setPadding(new Insets(10)); // Adjust padding as needed

        // Add hover effect - Border
        Border originalBorder = card.getBorder();
        Border hoverBorder = new Border(new javafx.scene.layout.BorderStroke(
                Color.GRAY,
                BorderStrokeStyle.SOLID,
                CornerRadii.EMPTY,
                new BorderWidths(1), // Adjust border width as needed
                Insets.EMPTY
        ));

        card.addEventFilter(MouseEvent.MOUSE_ENTERED, event -> card.setBorder(hoverBorder));
        card.addEventFilter(MouseEvent.MOUSE_EXITED, event -> card.setBorder(originalBorder));


        return card;
    }

    public List<DestinationEntity> findDestinationsByCriteria() {
        // Get values from JavaFX components
        TypeVoyageEnum selectedType = destinationTypeChoiseBox.getValue();
        PaysEnum selectedPays = destinationPaysChoiseBox.getValue();
        RegionEnum selectedRegion = destinationRegionChoiseBox.getValue();
        LocalDate dateDebut = destinationDateDebutDatePicker.getValue();
        LocalDate dateFin = destinationDateFinDatePicker.getValue();

        int placesDisponibles = 0;
        if (!StringUtils.isEmpty(destinationPlaceDispoTextField.getText())) {
            placesDisponibles = Integer.parseInt(destinationPlaceDispoTextField.getText());
        }
        double prixParPersonne = 0;
        if (!StringUtils.isEmpty(destinationPrixParPersonneTextField.getText())) {
            prixParPersonne = Double.parseDouble(destinationPrixParPersonneTextField.getText());
        }
        return destinationRepository.findAll(DestinationSpecifications.findByCriteria(
                selectedPays, selectedRegion, selectedType, dateDebut, dateFin, placesDisponibles, prixParPersonne));
    }

    @FXML
    public void filtrerDestination(ActionEvent actionEvent) {
        List<DestinationEntity> filteredDestinations = findDestinationsByCriteria();

        // Clear the existing destination cards and add new ones
        destinationCardsContainer.getChildren().setAll(filteredDestinations.stream()
                .map(this::createDestinationCard)
                .peek(destinationCard -> destinationCard.setFocusTraversable(true))
                .collect(Collectors.toList()));

        // Adjust the container height if needed

    }

    @FXML
    public void reloadAllDestinations(ActionEvent actionEvent) {
        List<DestinationEntity> allDestinations = destinationRepository.findAll();

        // Clear the existing destination cards and add new ones
        destinationCardsContainer.getChildren().setAll(allDestinations.stream()
                .map(this::createDestinationCard)
                .peek(destinationCard -> destinationCard.setFocusTraversable(true))
                .collect(Collectors.toList()));

        // Adjust the container height if needed


        // Clear the filter fields
        clearFilterFields();
    }

    @FXML
    public void clearFilterFields() {
        destinationTypeChoiseBox.setValue(null);
        destinationPaysChoiseBox.setValue(null);
        destinationRegionChoiseBox.setValue(null);
        destinationDateDebutDatePicker.setValue(null);
        destinationDateFinDatePicker.setValue(null);
    }


}
