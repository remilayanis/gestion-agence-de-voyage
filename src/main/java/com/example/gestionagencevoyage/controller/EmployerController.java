package com.example.gestionagencevoyage.controller;

import com.example.gestionagencevoyage.entity.EmployerEntity;
import com.example.gestionagencevoyage.entity.EtatCompteEnum;
import com.example.gestionagencevoyage.entity.RoleEnum;
import com.example.gestionagencevoyage.repository.EmployerRepository;
import com.example.gestionagencevoyage.utils.ActionColumnUtil;
import com.example.gestionagencevoyage.utils.GestionVoyageUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

@Component
public class EmployerController implements Initializable {

    @Autowired
    private EmployerRepository employerRepository;

    private EmployerEntity employerToEdit;
    private Stage employerFormStage;
    private boolean isConfigureActionColumn = false;

    @FXML
    private TableView<EmployerEntity> employerList;
    @FXML
    private TextField usernameTextField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private ComboBox<RoleEnum> roleComboBox;
    @FXML
    private ComboBox<EtatCompteEnum> etatComboBox;
    @FXML
    private Button employerFormButton;
    @FXML
    private Label formLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialize the controller and configure action column.
        if (!isConfigureActionColumn) {
            configureActionColumn();
            isConfigureActionColumn = true;
        }
        loadAllEmployers();
    }

    @FXML
    public void openAddEmployerForm() {
        employerToEdit = null;
        showEmployerForm("Ajouter", null);
    }

    public void openEditEmployerForm(EmployerEntity employer) {
        employerToEdit = employer;
        showEmployerForm("Modifier", employer);
    }

    private void showEmployerForm(String title, EmployerEntity employer) {
        try {
            Parent root = GestionVoyageUtils.loadFxml("/gestion_employer/formulaire_employer.fxml");

            employerFormStage = new Stage();
            employerFormStage.initModality(Modality.WINDOW_MODAL);
            employerFormStage.initOwner(employerList.getScene().getWindow());

            employerFormStage.setTitle(title + " un Employeur");
            formLabel.setText(title + " un Employeur");
            employerFormButton.setText(title);
            ObservableList<RoleEnum> roleList = FXCollections.observableArrayList(RoleEnum.values());
            roleComboBox.setItems(roleList);

            ObservableList<EtatCompteEnum> etatCompteList = FXCollections.observableArrayList(EtatCompteEnum.values());
            etatComboBox.setItems(etatCompteList);

            if (employer != null) {
                fillEmployerFormFields(employer);
            }

            employerFormButton.setOnAction(event -> {
                if (employerToEdit != null) {
                    updateEmployer();
                } else {
                    addEmployer();
                }
            });

            employerFormStage.setScene(new Scene(root));
            employerFormStage.showAndWait();
            loadAllEmployers();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fillEmployerFormFields(EmployerEntity employer) {
        usernameTextField.setText(employer.getUsername());
        passwordField.setText(employer.getPassword());
        roleComboBox.setValue(employer.getRole());
        etatComboBox.setValue(employer.getEtat());
    }

    public void addEmployer() {
        String usernameValue = usernameTextField.getText();
        String passwordValue = passwordField.getText();
        RoleEnum roleValue = roleComboBox.getValue();
        EtatCompteEnum etatValue = etatComboBox.getValue();

        String missingFields = checkRequiredFields(usernameValue, passwordValue, roleValue, etatValue);

        if (!missingFields.isEmpty()) {
            showEmptyFieldsAlert(missingFields);
            return;
        }

        EmployerEntity newEmployer = new EmployerEntity(usernameValue, passwordValue, roleValue, etatValue);
        employerRepository.save(newEmployer);

        clearFormFields();
        employerFormStage.close();
    }

    public void updateEmployer() {
        if (employerToEdit != null) {
            String usernameValue = usernameTextField.getText();
            String passwordValue = passwordField.getText();
            RoleEnum roleValue = roleComboBox.getValue();
            EtatCompteEnum etatValue = etatComboBox.getValue();

            String missingFields = checkRequiredFields(usernameValue, passwordValue, roleValue, etatValue);

            if (!missingFields.isEmpty()) {
                showEmptyFieldsAlert(missingFields);
                return;
            }

            employerToEdit.setUsername(usernameValue);
            employerToEdit.setPassword(passwordValue);
            employerToEdit.setRole(roleValue);
            employerToEdit.setEtat(etatValue);

            employerRepository.save(employerToEdit);
            employerToEdit = null;

            clearFormFields();
            employerFormStage.close();
        }
    }

    private void clearFormFields() {
        usernameTextField.clear();
        passwordField.clear();
        roleComboBox.setValue(null);
        etatComboBox.setValue(null);
    }

    private void configureActionColumn() {
        TableColumn<EmployerEntity, Void> actionColumn = ActionColumnUtil.create("Action", new ActionColumnUtil.ActionCallback<EmployerEntity>() {
            @Override
            public void onDetail(EmployerEntity item) {
                viewEmployerDetails(item);
            }

            @Override
            public void onEdit(EmployerEntity item) {
                openEditEmployerForm(item);
            }

            @Override
            public void onDelete(EmployerEntity item) {
                deleteEmployer(item);
            }
        });

        employerList.getColumns().add(actionColumn);
    }

    private void viewEmployerDetails(EmployerEntity employer) {
        try {
            Parent root = GestionVoyageUtils.loadFxml("/gestion_employer/detail_employer.fxml");

            Stage detailsStage = new Stage();
            detailsStage.initModality(Modality.WINDOW_MODAL);
            detailsStage.initOwner(employerList.getScene().getWindow());
            detailsStage.setTitle("Détails de l'Employeur");

            Label detailUsername = (Label) root.lookup("#detailUsername");
            Label detailPassword = (Label) root.lookup("#detailPassword");
            Label detailRole = (Label) root.lookup("#detailRole");
            Label detailEtat = (Label) root.lookup("#detailEtat");

            detailUsername.setText(employer.getUsername());
            detailPassword.setText(employer.getPassword());
            detailRole.setText(employer.getRole().toString());
            detailEtat.setText(employer.getEtat().toString());

            Button detailBtnFermer = (Button) root.lookup("#detailBtnFermer");
            detailBtnFermer.setOnAction(event -> detailsStage.close());

            detailsStage.setScene(new Scene(root));
            detailsStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteEmployer(EmployerEntity employer) {
        employerRepository.delete(employer);
        loadAllEmployers();
    }

    private void loadAllEmployers() {
        List<EmployerEntity> employers = employerRepository.findAll();
        employerList.getItems().setAll(employers);
    }

    private String checkRequiredFields(String username, String password, RoleEnum role, EtatCompteEnum etat) {
        String missingFields = "";

        if (username.isEmpty()) {
            missingFields += "- Nom d'utilisateur\n";
        }
        if (password.isEmpty()) {
            missingFields += "- Mot de passe\n";
        }
        if (role == null) {
            missingFields += "- Rôle\n";
        }
        if (etat == null) {
            missingFields += "- État du compte\n";
        }

        return missingFields;
    }

    private void showEmptyFieldsAlert(String missingFields) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Champs requis");
        alert.setHeaderText("Veuillez remplir tous les champs requis:");
        alert.setContentText(missingFields);
        alert.showAndWait();
    }
}
