package com.example.gestionagencevoyage;

import com.example.gestionagencevoyage.controller.HomePageController;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;


@Configuration
@SpringBootApplication
public class GestionAgenceVoyageApp extends Application {
    private static ConfigurableApplicationContext configurableApplicationContext;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws IOException {
        HomePageController.loadView(stage);
    }

    @Override
    public void init() {
        this.configurableApplicationContext = SpringApplication.run(GestionAgenceVoyageApp.class);
    }

    @Override
    public void stop() {
        this.configurableApplicationContext.close();
    }

    public static ConfigurableApplicationContext getConfigurableApplicationContext() {
        return configurableApplicationContext;
    }

}